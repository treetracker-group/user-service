package com.treetracker.service.impl;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.ArrayList;

public class JwtUserDetailsServiceImpl implements UserDetailsService {

  @Override
  public UserDetails loadUserByUsername(String username) {
    if ("springboot".equals(username)) {
      return new User(
          "springboot",
          "$2a$10$WD.TfCCwkKpZbqAqudCTjegJq/w9KN7rk0jxPgFaqpSNstA5FKK4S",
          new ArrayList<>());
    }
    throw new UsernameNotFoundException("User not found with username: " + username);
  }
}
