package com.treetracker.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.Serializable;

@AllArgsConstructor
@Getter
public class JwtRequest implements Serializable {

  private String username;
  private String password;
}
