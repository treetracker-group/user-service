package com.treetracker.configuration;

import com.treetracker.service.impl.JwtUserDetailsServiceImpl;
import com.treetracker.utils.JwtTokenUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.UserDetailsService;

@Configuration
public class ApplicationConfiguration {
  @Bean
  public JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint() {
    return new JwtAuthenticationEntryPoint();
  }

  @Bean
  public JwtRequestFilter jwtRequestFilter(
      JwtTokenUtil jwtTokenUtil, UserDetailsService userDetailsService) {
    return new JwtRequestFilter(jwtTokenUtil, userDetailsService);
  }

  /*@Bean
  public WebSecurityConfig webSecurityConfig(
      JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint,
      UserDetailsService userDetailsService,
      JwtRequestFilter jwtRequestFilter) {
    return new WebSecurityConfig(jwtAuthenticationEntryPoint, userDetailsService, jwtRequestFilter);
  }*/

  @Bean
  public JwtTokenUtil jwtTokenUtil() {
    return new JwtTokenUtil();
  }

  @Bean
  public UserDetailsService jwtUserDetailsService() {
    return new JwtUserDetailsServiceImpl();
  }
}
